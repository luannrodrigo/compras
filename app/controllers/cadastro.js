  module.exports.cadastro = function(application, req, res){
  res.render('admin/cadastro', {validacao: {}});
};

module.exports.cadastrar = function(application, req, res){
  // Pegando os erros do form e atribuindo seus valores a variacael dados form
  let dadosForm = req.body;

  // Tratando erros
  req.assert('nome',  'Campo nome não pode ser vazio').notEmpty()
  req.assert('login', 'Campo login não pode ser vazio').notEmpty()
  req.assert('senha', 'Campo senha não pode ser vazio').notEmpty()
  req.assert('email', 'Campo E-mail não pode ser vazio').notEmpty()

  // mostrando erros
  let erros = req.validationErrors();
  if (erros) {
    res.render('admin/cadastro', {validacao: erros});
    return;
  }
  // paramentros para conexão com o banco
  const dbConnection = new application.config.dbConnection();
  // obj para inserção no banco
  const cadastroDAO = new application.app.models.cadastroDAO(dbConnection);
  // passando os dados do form para as propridades que estão no cadastroDAO
  cadastroDAO.inserirUsuario(dadosForm);
  res.send('ok');

};
