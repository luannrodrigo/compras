module.exports.index = function(application, req, res){
  res.render('index',{validacao: {}});
};

module.exports.auth = function(application, req, res) {
  // Pegando as informações do formulario e atribindo a variavel dadosForm
  let dadosForm = req.body
  console.log(dadosForm);

  req.assert('login', 'Campo login não pode ser vazio').notEmpty()
  req.assert('senha', 'Campo senha não pode ser vazio').notEmpty()

  // mostrando erros
  let erros = req.validationErrors();
  if (erros) {
    res.render('index', {validacao: erros});
    return;
  }
  // paramentros para conexão com o banco
  const dbConnection = new application.config.dbConnection();
  // obj para inserção no banco
  const cadastroDAO = new application.app.models.cadastroDAO(dbConnection);
  // passando os dados do form para as propridades que estão no cadastroDAO
  authDAO.autenticar(dadosForm, req, res);

};
