function cadastroDAO(dbConnection){
    this._dbConnection = dbConnection;
}
cadastroDAO.prototype.inserirUsuario = function(datas){
    var mongoConnected = this._dbConnection.connectMongo(function(client, db){
        var collection = db.collection('usuarios');
        collection.insert(datas);
        client.close;
    });
}
module.exports = function(){
    return cadastroDAO;
}
