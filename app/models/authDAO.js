function authDAO(dbConnection) {
  this._dbConnection = dbConnection;
}
authDAO.prototype.autenticar = function(datas, req, res){
  const mongoConnected = this._dbConnection.connectMongo(function(client, db) {
    const collection = db.collection('usuarios');
    collection.find(datas).toArray(function(err, result){
      if(result[0] != undefined){
        req.session.autorizado = true;
      }
      if(req.session.autorizado){
        res.send("ok, auth")
      }else{
        res.render('index', {validacao: {}});
      }
    });
      client.close();
  })
}
