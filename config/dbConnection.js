var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://localhost:27017';
var dbName = 'compras';

function dbConnection(){
    this._MongoClient = undefined;
    this._MongoDB = undefined;
}

dbConnection.prototype.connectMongo = function(connection){
    MongoClient.connect(url, function(err, client){
    var MongoClient = client;
    var MongoDB = client.db(dbName);

    return connection(MongoClient, MongoDB);
    });
}
module.exports = function(){
    return dbConnection;
}


// // Variavel que contem os paramentos de conexão com o banco
// const connMongoDB = function(){
//   const db = new mongo.Db(
//     'compras', //nome do banco
//     new mongo.Server(
//       '127.0.0.1',//ip do banco
//       '27017',//porta padrão do mongo
//       {}//configurações adicionais
//     ),
//     {}, //configurações adicionais
//   );
//   return db;
// }
// module.exports = function(){
//   return connMongoDB;
// };
//
//
